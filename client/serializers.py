from rest_framework import serializers
from .models import Evento
from django.contrib.auth.models import User


class EventoSerializer(serializers.ModelSerializer):  # create class to serializer model

    class Meta:
        model = Evento
        fields = ('id', 'nome', 'descrizione', 'keyword', 'cultura', 'cibo', 'movida', 'natura', 'relax', 'inizio', 'fine')
