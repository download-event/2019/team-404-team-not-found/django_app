from django.conf.urls import url
from django.urls import include, path, re_path
from . import views


urlpatterns = [
    re_path(r'^api/v1/event/(?P<pk>[0-9]+)$', # Url to get update event
        views.get_update_event,
        name='get_update_event'
    ),
    path('api/v1/event/', # urls list all and create new one
        views.get_post_event.as_view(),
        name='get_post_event'
    ),
    path('api/v1/get_interessi', # urls list all and create new one
        views.get_interessi,
        name='get_interessi'
    ),
    path('api/v1/set_interessi_init', # urls list all and create new one
        views.set_interessi_init,
        name='set_interessi_init'
    ),
    url(r'^api/v1/search_from_key/$', # urls list all and create new one
        views.search_from_key,
        name='search_from_key'
    )
]