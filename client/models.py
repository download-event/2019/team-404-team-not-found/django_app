from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Interessi(models.Model):
    cultura = models.IntegerField()
    cibo = models.IntegerField()
    movida = models.IntegerField()
    natura = models.IntegerField()
    relax = models.IntegerField()
    utente = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )


class Doracoin(models.Model):
    value = models.IntegerField()
    utente = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )


class Evento(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=100)
    descrizione = models.CharField(max_length=500)
    keyword = models.CharField(max_length=200)
    cultura = models.BooleanField(default=False)
    cibo = models.BooleanField(default=False)
    movida = models.BooleanField(default=False)
    natura = models.BooleanField(default=False)
    relax = models.BooleanField(default=False)
    inizio = models.DateTimeField(auto_now=False, auto_now_add=False)
    fine = models.DateTimeField(auto_now=False, auto_now_add=False)


class Prenotazioni(models.Model):
    id = models.AutoField(primary_key=True)
    ora = models.DateTimeField(auto_now=False, auto_now_add=True)
    utente = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    evento = models.ForeignKey(
        Evento,
        on_delete=models.CASCADE
    )



