import json
from django.http import HttpResponse
from django.core import serializers
from rest_framework import status, views
from rest_framework.decorators import api_view, permission_classes
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView
from .models import Evento, Interessi
from .permissions import IsOwnerOrReadOnly, IsAuthenticated
from .serializers import EventoSerializer
from rest_framework import permissions


@api_view(['GET'])
def get_interessi(request):
    interessi_as_json = serializers.serialize('json', Interessi.objects.filter(utente_id=request.user.id))
    dataInt = json.loads(interessi_as_json)
    count= 0
    count += dataInt[0]['fields']['cultura']
    count += dataInt[0]['fields']['cibo']
    count += dataInt[0]['fields']['movida']
    count += dataInt[0]['fields']['natura']
    count += dataInt[0]['fields']['relax']
    response = []
    if count is 0:
        return Response([0, 0, 0, 0, 0], status=status.HTTP_201_CREATED)
    print(dataInt[0]['fields']['cultura']/count)
    response.append(dataInt[0]['fields']['cultura']/count)
    response.append(dataInt[0]['fields']['cibo']/count)
    response.append(dataInt[0]['fields']['movida']/count)
    response.append(dataInt[0]['fields']['natura']/count)
    response.append(dataInt[0]['fields']['relax']/count)
    return Response(response, status=status.HTTP_201_CREATED)


@api_view(['GET'])
def set_interessi_init(request):
    interessi = Interessi(utente=request.user, cultura=0, cibo=0, movida=0, natura=0, relax=0)
    interessi.save()
    return Response("ok", status=status.HTTP_201_CREATED)


@api_view(['GET'])
def get_update_event(request, pk):
    event_as_json = serializers.serialize('json', Evento.objects.filter(id=pk))
    return HttpResponse(event_as_json, content_type='json')


class get_post_event(views.APIView):
    def get(self, request):
        event_as_json = serializers.serialize('json', Evento.objects.all())
        return HttpResponse(event_as_json, content_type='json')
    def post(self, request):
        if request.user.username == 'admin':
            data = request.data
            event = Evento(nome=data['nome'], descrizione=data['descrizione'], keyword=data['keyword'],
                           cultura=data['cultura'], cibo=data['cibo'], movida=data['movida'], natura=data['natura'],
                           relax=data['relax'], inizio=data['inizio'], fine=data['fine'])
            event.save()
            return Response("ok", status=status.HTTP_201_CREATED)
        return Response("error", status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def search_from_key(request):
    coord = {0: "45.71284343144624,9.66961468065574",
            1: "45.70704174314249,9.659108991813355",
            2: "45.69668351268485,9.643796784416281",
            3: "45.686323363154024,9.69783577330827",
            4: "45.69006475136607,9.662404839959436",
            5: "45.691695534671595,9.653615764886581"}

    keys = request.GET.get('keys', '')
    key = keys.split('%')
    event_as_json = serializers.serialize('json', Evento.objects.all())
    data = json.loads(event_as_json)
    keyword = ['cultura', 'cibo', 'movida', 'natura', 'relax']
    dict_key = {}

    for event in data:
        keyword.append(event["fields"]["nome"])
        array = [event["fields"]["nome"]]
        keywordsev = event["fields"]["keyword"].split(";")
        for keyw in keywordsev:
            array.append(keyw)
            keyword.append(keyw)
        if event["fields"]["cultura"]:
            array.append("cultura")
        if event["fields"]["cibo"]:
            array.append("cibo")
        if event["fields"]["movida"]:
            array.append("movida")
        if event["fields"]["natura"]:
            array.append("natura")
        if event["fields"]["relax"]:
            array.append("relax")
        dict_key[event["pk"]] = array
        if keys.strip().lower() in event["fields"]["nome"].lower():
            response = []
            dict2 = {}
            dict2['id'] = event["pk"]
            dict2['coord'] = coord[event["pk"]]
            dict2['nome'] = event["fields"]["nome"]
            dict2['keyword'] = event["fields"]["keyword"]
            response.append(dict2)
            if len(response) > 0:
                update_stat(request.user.id, response)
            return Response(response, status=status.HTTP_201_CREATED)
    newkeyword = list(dict.fromkeys(array))

    for id, value in dict_key.items():
        i = 0
        for singlekey in key:
            if singlekey in value:
                i = i + 1
        dict_key[id] = i
    import operator
    sorted_x = sorted(dict_key.items(), key=operator.itemgetter(1), reverse=True)
    response = []
    for event in sorted_x:
        if event[1] == 0:
            continue
        dict2={}
        evento = serializers.serialize('json', Evento.objects.filter(id=event[0]))
        dataEV = json.loads(evento)
        print(event[0])
        dict2['coord'] = coord[event[0]]
        dict2['id'] = event[0]
        dict2['nome'] = dataEV[0]["fields"]["nome"]
        dict2['keyword'] = dataEV[0]["fields"]["keyword"]
        response.append(dict2)

    if len(response) > 0:
        update_stat(request.user.id, response)
    return Response(response, status=status.HTTP_201_CREATED)


def update_stat(id, response):
    idevent = response[0]['id']
    evento = serializers.serialize('json', Evento.objects.filter(id=idevent))
    dataEV = json.loads(evento)
    int = Interessi.objects.get(utente_id=id)
    print(dataEV[0]["fields"])
    if dataEV[0]["fields"]["cultura"]:
        int.cultura = int.cultura + 1
    if dataEV[0]["fields"]["cibo"]:
        int.cibo = int.cibo + 1
    if dataEV[0]["fields"]["movida"]:
        int.movida = int.movida + 1
    if dataEV[0]["fields"]["natura"]:
        int.natura = int.natura + 1
    if dataEV[0]["fields"]["relax"]:
        int.relax = int.relax + 1
    int.save()
